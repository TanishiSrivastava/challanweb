var firebase = require("firebase");
var thunkify = require("thunkify");

var firebaseConfig = {
    apiKey: "AIzaSyCGx9vM-SACsL8azO4F0RibObyioeSMzF4",
    authDomain: "challan-b5438.firebaseapp.com",
    databaseURL: "https://challan-b5438.firebaseio.com",
    projectId: "challan-b5438",
    storageBucket: "challan-b5438.appspot.com",
    messagingSenderId: "856388157810",
    appId: "1:856388157810:web:5334c0cbdf145fd5"
  };

  firebase.initializeApp(firebaseConfig);

function getUserIdByEmail(email,password){
    return firebase.auth().signInWithEmailAndPassword(email,password).then(function(user){
        return user.user.uid;
    }).catch(function(err){
        return null;
    });
}


function addChallan(uid,rid,date){
    firebase.database().ref("challan/").push({
        uid:uid,
        rid:rid,
        date:date,
        status:false
    });
}

function confirmChallan(challanId,uid,rid,date){
    return firebase.database().ref("challan/").once("value").then(function(data){
        data = data.val();
        var challan = [];
        var serial = [];
        for(var i in data){
            if(data[i].uid===uid){
                challan.push(data[i]);
                serial.push(i);
            }
        }
        var value = challan[challanId];
        value.uid = uid;
        value.rid = rid;
        value.date = date;
        value.status = true;
        var key = serial[challanId];
        console.log(key,"key")
        return firebase.database().ref("challan/"+key).set(value).then(function(metadata){
            return metadata;
        });
    });
}

function addRoute(from,to,cost){
    firebase.database().ref("routes/").push({
        from:from,
        to:to,
        cost:cost
    });
}

function createUser(email,password){
    return firebase.auth().createUserWithEmailAndPassword(email,password).then(function(user){
        return user.user.uid;
    });
}

function addUser(uid,name,licence,dob,phone){
    firebase.database().ref("users/"+uid).set({
        name:name,
        licence:licence,
        dob:dob,
        phone:phone
    });
}

function getRoutes(){
    return firebase.database().ref("routes/").once("value").then(function(data){
        return data.val();
    });
}

function getChallans(){
    return firebase.database().ref("challan/").once("value").then(function(challans){
        return challans.val();
    });
}

function getChallansById(uid){
    return firebase.database().ref("challan/").once("value").then(function(challans){
        challans = challans.val();
        var challan = [];
        // console.log("challans in firebase",challans);
        for(var i in challans){
            if(challans[i].uid == uid) challan.push(challans[i]);
        }
        return challan;
    });
}

function getChallanById(id,uid){
    return firebase.database().ref("challan/").once("value").then(function(challans){
        var i = [];
        challans = challans.val();
        for(var j in challans){
            if(challans[j].uid == uid) i.push(challans[j]);
            console.log(j,challans[j],"loop");
        }

        var c = i[id];
        c["id"] = id;

        return c;
    });
}

function getUser(uid){
    return firebase.database().ref("users/"+uid).once("value").then(function(user){
        var retData = user.val();
        retData['id'] = uid;
        return retData;
    });
}
function getUsers(){
    return firebase.database().ref("users/").once("value").then(function(users){
        return users.val();
    });
}

function postProcedure(){
    return firebase.auth().signOut().then(function(msg){
        return msg;
    });
}


  module.exports = {
    addChallan:addChallan,
    addRoute:addRoute,
    createUser:createUser,
    addUser:addUser,
    getRoutes:getRoutes,
    getChallans:getChallans,
    getUser:getUser,
    getUsers:getUsers,
    getUserIdByEmail:getUserIdByEmail,
    postProcedure:postProcedure,
    getChallansById:getChallansById,
    confirmChallan:confirmChallan,
    getChallanById:getChallanById
  }