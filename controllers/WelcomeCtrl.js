var firebaseUtils = require("../utils/firebaseUtils");


module.exports = {
    showHomePage: function* (next) {
        yield this.render('home',{

        });
    },
    showLoginPage: function* (next) {
        yield this.render('login',{

        });
    },
    showSignUpPage: function* (next) {
        yield this.render('signup',{

        });
    },
    login:function*(next){
        var email = this.request.body.email;
        var password = this.request.body.password;
        var user = yield firebaseUtils.getUserIdByEmail(email,password);
        yield firebaseUtils.postProcedure();
        console.log(user,user==null);
        if(user!=null){
            this.cookies.set("SESSION_ID",user);
            this.redirect("/dash");
        }else{
            this.redirect("/login");
        }
    },
    logout:function*(next){
        console.log("came here");
        this.cookies.set("SESSION_ID", '', {expires: new Date(1), path: '/'});
        this.redirect("/");
    },
    showDash:function*(next){
        console.log(this.currentUser);
        if(this.currentUser){
            var data = yield firebaseUtils.getChallansById(this.currentUser.id);
            var routes = yield firebaseUtils.getRoutes();
            var rs = {};
            for(var i in routes){
                rs[routes[i].id] = routes[i];
            }
            yield this.render("dash",{
                challans:data,
                routes:rs
            })
        }else{
            this.redirect("/");
        }
    },
    signup:function*(next){
        var name = this.request.body.name;
        var email = this.request.body.email;
        var password = this.request.body.password;
        var phone = this.request.body.phone;
        var licence = this.request.body.licence;
        var dob = this.request.body.dob;
        var uid = yield firebaseUtils.createUser(email,password);
        firebaseUtils.addUser(uid,name,licence,dob,phone);
        this.redirect("/login");
    },
    newChallan:function*(next){
        if(this.currentUser){
            yield this.render("createchallan",{

            });
        }else{
            this.redirect("/dash");
        }
    },
    confirmChallan:function*(next){
        var cid = this.request.body.challanId;
        var uid =  this.currentUser.id;
        var challan = yield firebaseUtils.getChallanById(cid,uid);
        
        console.log(challan);

        yield this.render("confirmchallan",{
            cid:challan.id,
            cdate:challan.date
        });
    },
    newChallanPost:function*(next){
        var rid = this.request.body.routeId;
        var date = this.request.body.date;
        var uid = this.currentUser.id;
        firebaseUtils.addChallan(uid,rid,date);
        this.redirect("/dash");
    },
    getAllRoutes:function*(next){
        var routes = yield firebaseUtils.getRoutes();
        this.body = routes;
    },
    editNewChallan:function*(next){

        console.log("yahaan aaya");

        var rid = this.request.body.routeId;
        var date = this.request.body.date;
        var uid = this.currentUser.id;
        var cid = this.request.body.cid;

        console.log( cid,uid,rid,date )

        yield firebaseUtils.confirmChallan(cid,uid,rid,date);
        this.redirect("/dash");
    },
    viewChallan:function*(next){
        var cid = this.params.cid;
        var uid = this.currentUser.id;
        var challan = yield firebaseUtils.getChallanById(cid,uid);

        console.log(challan,"inside viewChallan");

        yield this.render("viewChallan",{challan:challan});
    }
    }
