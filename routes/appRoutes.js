var Router= require('koa-router');
var bodyParser = require('koa-body')();

module.exports = function(app){

    var router = new Router();

    //Welcome Routes
    var welcomeCtrl = require('./../controllers/WelcomeCtrl');
    router.get('/', welcomeCtrl.showHomePage);
    router.get("/login",welcomeCtrl.showLoginPage);
    router.get('/signup', welcomeCtrl.showSignUpPage);
    router.get("/logout",welcomeCtrl.logout);
    router.get("/dash",welcomeCtrl.showDash);
    router.get("/newChallan",welcomeCtrl.newChallan);
    router.get("/viewChallan/:cid",welcomeCtrl.viewChallan);



    router.post("/confirmChallan",welcomeCtrl.confirmChallan);
    router.post("/login",welcomeCtrl.login);
    router.post("/signup",welcomeCtrl.signup);
    router.post("/newChallan",welcomeCtrl.newChallanPost);
    router.post("/getAllRoutes",welcomeCtrl.getAllRoutes);
    router.post("/editNewChallan",welcomeCtrl.editNewChallan);
    
    
    return router.middleware();
}
